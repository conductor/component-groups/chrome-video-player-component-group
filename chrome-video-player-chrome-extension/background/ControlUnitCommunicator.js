/**
 * Communicates with the Java application over web socket
 * (which in turn communicates with the conductor).
 * Receives commands such as play, pause, seek, volume from the server
 * and sends back the status of the video player to the control unit.
 * Note: We only send back the status of one video player which is
 * the one that was last in use.
 */
ControlUnitCommunicator = (function() {
    var mURL = "ws://127.0.0.1:9001"; // URL of the control unit (server)

    init();

    /**
     * Set up event listeners
     */
    function init() {
        ClientSocket.addEventListener('onMessage', onMessageReceived);
        ClientSocket.addEventListener('onClose', onClose);
        ClientSocket.connect(mURL);

        VideoPlayer.addEventListener('isPlaying', onPlayerStatusChanged);
        VideoPlayer.addEventListener('volumeChanged', onPlayerVolumeChanged);
        VideoPlayer.addEventListener('durationChanged', onPlayerDurationChanged);
        VideoPlayer.addEventListener('currentTimeChanged', onPlayerCurrentTimeChanged);
        VideoPlayer.addEventListener('isMutedChanged', onPlayerIsMutedChanged);
        VideoPlayer.addEventListener('videoUrlChanged', onPlayerVideoUrlChanged);
    }

    /**
     * Event that is triggered when the video player has changed status
     *
     * @param isPlaying  true if the video player is currently playing otherwise return false
     */
    function onPlayerStatusChanged(isPlaying) {
        ClientSocket.sendJSON({
            "propertyName": "playing",
            "propertyValue": isPlaying
        });
    }

    /**
     * Event that is triggered when the volume of the video player is changed
     *
     * @param volume  the new volume of the video player, the value is between 0 and 100
     */
    function onPlayerVolumeChanged(volume) {
        ClientSocket.sendJSON({
            "propertyName": "volume",
            "propertyValue": volume
        });
    }

    /**
     * Event that is triggered when the video players video duration (video length) has changed
     *
     * @param duration  the duration of the video in seconds
     */
    function onPlayerDurationChanged(duration) {
        ClientSocket.sendJSON({
            "propertyName": "duration",
            "propertyValue": duration
        });
    }

    /**
     * Event that is triggered when the current seek position has changed of the video player
     *
     * @param currentTime  the current time (seek position), in seconds
     */
    function onPlayerCurrentTimeChanged(currentTime) {
        ClientSocket.sendJSON({
            "propertyName": "currentTime",
            "propertyValue": currentTime
        });
    }

    /**
     * Event that is triggered when the video player is muted or unmuted
     *
     * @param isMuted  true if the video player is now muted or false if the player is unmuted
     */
    function onPlayerIsMutedChanged(isMuted) {
        ClientSocket.sendJSON({
            "propertyName": "muted",
            "propertyValue": isMuted
        });
    }

    /**
     * Event that is triggered when the video URL of the video player has changed
     *
     * @param videoUrl  the URL of the new video that is playing
     */
    function onPlayerVideoUrlChanged(videoUrl) {
        ClientSocket.sendJSON({
            "propertyName": "videoUri",
            "propertyValue": videoUrl
        });
    }

    /**
     * Event that is triggered when the connection to the server (control unit) disconnects
     *
     * @param event  the event that was thrown from the client socket
     */
    function onClose(event) {
        // Try to reconnect to the server after 3 seconds
        setTimeout(function() {
            ClientSocket.connect(mURL);
        }, 3000);
    }

    /**
     * Event that is triggered when a message is received from the server (control unit)
     *
     * @param event  the event that was casted from the client socket
     */
    function onMessageReceived(event) {
        try {
            var message = event.data;
            var json = JSON.parse(message);

            if (json.errorMessage) {
                console.log("Error message received from server (control unit), error message was: " + json.errorMessage);
            } else if (json.propertyName || json.propertyValue !== undefined) {
                setPropertyValue(json.propertyName, json.propertyValue);
            } else {
                console.error("Invalid incoming data from server, expected data to contain propertyName and propertyValue");
            }
        } catch (e) {
            console.error("onMessageReceived, Exception occurred with message: " + e.message, e);
        }
    }

    /**
     * Send command to the video player
     *
     * @param propertyName   the name of the property that is to be set
     * @param propertyValue  the value of the property that is to be set
     */
    function setPropertyValue(propertyName, propertyValue) {
        if (propertyName === "playing") {
            Assert.isBoolean(propertyValue, "Invalid propertyValue, expected boolean value for propertyName '" + propertyName + "'");
            var play = propertyValue;

            if (play) {
                VideoPlayer.play();
            } else {
                VideoPlayer.pause();
            }
        }
    }
}());