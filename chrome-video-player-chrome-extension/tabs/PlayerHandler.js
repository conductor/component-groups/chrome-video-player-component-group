﻿/**
 * PlayerHandler handles all the video players that exist in one tab.
 * It receives events such as paused, playing and can send commands to a
 * video player to stop playback and to resume playback
 */
PlayerHandler = (function() {
    var mPlayers = []; // Array containing all video players in this tab
    var mEventListeners = [];

    /**
     * Add a new video player to the collection
     *
     * @param playerId  the id that the new video player should have
     * @param videoId   the video video id that the new player should load
     * @param autoplay  auto start playback when the new player has finished loading
     */
    function addPlayer(player) {
        var playerId = HelpFunctions.createGUID(); //Create a unique id for our new video player

        console.log('Adding player (' + playerId + ').', player);

        player.dataset.playerId = playerId;
        mPlayers[playerId] = player;

        castEvent("onPlayerAdded", {
            playerId: playerId
        });

        player.addEventListener("pause", onPaused, false);
        player.addEventListener("playing", onPlaying, false);
    }

    function onPaused(event) {
        var player = event.target;
        var playerId = getPlayerId(event.target);

        castEvent("onPlayerStateChanged", {
            player: player,
            playerId: playerId,
            playerState: 'paused',
            originalEvent: event
        });
    }

    function onPlaying(event) {
        var player = event.target;
        var playerId = getPlayerId(event.target);

        castEvent("onPlayerStateChanged", {
            player: player,
            playerId: playerId,
            playerState: 'playing',
            originalEvent: event
        });
    }

    function getStatus(playerId) {
        if (!existsPlayer(playerId)) {
            console.log("Unable to play video, couldn't find player with id: " + playerId);
            return;
        }

        var player = getPlayer(playerId);

        if (player.paused) {
            return 'paused';
        } else {
            return 'playing';
        }
    }

    /**
     * Check if a video player exists with a given id
     *
     * @param playerId  the id of the video player that is to be checked
     * @return          true if the video player exists, otherwise return false
     */
    function existsPlayer(playerId) {
        return getPlayer(playerId) !== null && getPlayer(playerId) !== undefined;
    }

    /**
     * Send command to a video player to start playing
     *
     * @param playerId  the id of the video player that should start playback
     */
    function play(playerId) {
        if (!existsPlayer(playerId)) {
            throw new Error("Couldn't find player with id: " + playerId);
            return;
        }

        var player = getPlayer(playerId);
        player.play();
    }

    /**
     * Send command to video player to pause playback
     *
     * @param playerId  the id of the video player that should pause playback
     */
    function pause(playerId) {
        if (!existsPlayer(playerId)) {
            throw new Error("Couldn't find player with id: " + playerId);
            return;
        }

        var player = getPlayer(playerId);
        player.pause();
    }

    /**
     * Get the length of the current video of the specified video player
     *
     * @param playerId  the id of the video player that we want the video duration of
     * @return          the video duration (video length) in seconds
     */
    function getDuration(playerId) {
        if (!existsPlayer(playerId)) {
            throw new Error("Couldn't find player with id: " + playerId);
            return;
        }

        return getPlayer(playerId).duration;
    }

    /**
     * Get the current volume of the specified video player
     *
     * @param playerId  the id of the video player that we want the current volume of
     * @return          a value between 0 and 100 that represent the current volume
     */
    function getVolume(playerId) {
        if (!existsPlayer(playerId)) {
            throw new Error("Couldn't find player with id: " + playerId);
            return;
        }

        return getPlayer(playerId).volume;
    }

    /**
     * Set the volume of a given video player
     *
     * @param playerId  the id of the video player that should change its volume
     * @param volume    the new volume that should be set
     */
    function setVolume(playerId, volume) {
        if (!existsPlayer(playerId)) {
            throw new Error("Couldn't find player with id: " + playerId);
            return;
        }

        return getPlayer(playerId).volume = volume;
    }

    /**
     * Check if a given video player is muted or not
     *
     * @param playerId  the id of the video player that we want to check if
     *                  muted or not
     * @return          true if the video player is muted, otherwise return false
     */
    function isMuted(playerId) {
        if (!existsPlayer(playerId)) {
            throw new Error("Couldn't find player with id: " + playerId);
            return;
        }

        return getPlayer(playerId).muted;
    }

    /**
     * Mute a specific video player
     *
     * @param playerId  the id of the video player that is to be muted
     */
    function mute(playerId) {
        if (!existsPlayer(playerId)) {
            throw new Error("Couldn't find player with id: " + playerId);
            return;
        }

        return getPlayer(playerId).muted = true;
    }

    /**
     * UnMute a specific video player
     *
     * @param playerId  the id of the video player that is to be unmuted
     */
    function unMute(playerId) {
        if (!existsPlayer(playerId)) {
            throw new Error("Couldn't find player with id: " + playerId);
            return;
        }

        return getPlayer(playerId).muted = false;
    }

    /**
     * Get the current video video URL of the specified video player
     *
     * @param playerId  the id of the video player
     * @return          the URL of the currently loaded video
     */
    function getVideoUrl(playerId) {
        if (!existsPlayer(playerId)) {
            throw new Error("Couldn't find player with id: " + playerId);
            return;
        }

        return getPlayer(playerId).currentSrc;
    }

    /**
     * Get the current time (seek position) of the specified video player
     *
     * @param playerId  the id of the video player that we want the current time of
     * @return          the number of seconds that has passed since the start of the video
     */
    function getCurrentTime(playerId) {
        if (!existsPlayer(playerId)) {
            throw new Error("Couldn't find player with id: " + playerId);
            return;
        }

        return getPlayer(playerId).currentTime;
    }

    /**
     * Get the video player object with a specific id
     *
     * @param playerId  the id of the video player that we want to retrieve
     * @return          a video player object
     */
    function getPlayer(playerId) {
        return mPlayers[playerId];
    }

    function getPlayerId(player) {
        return player.dataset.playerId;
    }

    /**
     * Get the ids of all video players in this tab
     *
     * @return  an array containing all player ids
     */
    function getAllPlayerIds() {
        return Object.keys(mPlayers);
    }

    /**
     * Add a new event listener which will be triggered when an event
     * with a certain name is cast
     *
     * @param eventName  name of the event that we want to listen for
     * @param listener   a function which will be called when an event
     *                   is triggered.
     */
    function addEventListener(eventName, listener) {
        console.log("addEventListener: " + eventName);

        if (mEventListeners[eventName] === undefined)
            mEventListeners[eventName] = [];

        mEventListeners[eventName].push(listener);
    }

    /**
     * Cast an event to all event listeners
     *
     * @param eventName   the name of the event
     * @param eventValue  the value of the event
     */
    function castEvent(eventName, eventValue) {
        console.log("Casting event,  eventName: " + eventName + ", eventValue: " + eventValue);

        if (mEventListeners[eventName] === undefined) {
            return;
        }

        for (var i = 0; i < mEventListeners[eventName].length; i++) {
            mEventListeners[eventName][i](eventValue);
        }
    }

    // Return public functions
    return {
        addEventListener: addEventListener,
        addPlayer: addPlayer,
        play: play,
        pause: pause,
        existsPlayer: existsPlayer,
        getAllPlayerIds: getAllPlayerIds,
        getDuration: getDuration,
        getVolume: getVolume,
        setVolume: setVolume,
        isMuted: isMuted,
        mute: mute,
        unMute: unMute,
        getVideoUrl: getVideoUrl,
        getCurrentTime: getCurrentTime,
        getStatus: getStatus
    }
}());