/**
 * Handles communication with the background script.
 * Sends back statuses (volume, current seek time, duration, is playing, current track)
 * of this video player to the background script
 * and receives commands such as play, pause from the background script.
 */
Communicator = (function() {

    init();

    /**
     * Set up event listeners
     */
    function init() {
        PlayerHandler.addEventListener("onPlayerAdded", onPlayerAdded);
        PlayerHandler.addEventListener("onPlayerStateChanged", onPlayerStateChanged);
    }

    /**
     * Event that is triggered when a new video player has been added
     * and is ready to receive commands
     *
     * @param event  the event that was sent from the playerHandler
     */
    function onPlayerAdded(event) {
        console.log('Player added event.', event);
        var playerId = event.playerId;

        var volume = PlayerHandler.getVolume(playerId);
        var isMuted = PlayerHandler.isMuted(playerId);
        var duration = PlayerHandler.getDuration(playerId);
        var videoUrl = PlayerHandler.getVideoUrl(playerId);
        var currentTime = PlayerHandler.getCurrentTime(playerId);
        var status = PlayerHandler.getStatus(playerId);

        chrome.extension.sendRequest({
            player: {
                playerId: playerId,
                event: "added",
                status: status,
                volume: volume,
                isMuted: isMuted,
                duration: duration,
                videoUrl: videoUrl,
                currentTime: currentTime
            }
        }, function(response) {});
    }

    /**
     * Event that is triggered when a video player has changed state
     * (buffering, playing, paused etc).
     *
     * @param event  the event that was sent from the playerHandler
     */
    function onPlayerStateChanged(event) {
        console.log('Player state changed.', event);
        chrome.extension.sendRequest({
            player: {
                playerId: event.playerId,
                status: event.playerState,
                event: "statusChanged"
            }
        }, function(response) {}); //Send message to the background script that a video player has changed state
    }

    /**
     * Listen for commands from the background script to pause, play etc.
     *
     * @param request       the request that was sent from the background script
     * @param sender        the one who sent the request
     * @param sendResponse  callback function
     */
    chrome.runtime.onMessage.addListener(function(request, sender, sendResponse) {
        var playerId = request.playerId;
        var setStatus = request.setStatus;

        if (playerId && setStatus) {
            if (setStatus === "play") {
                PlayerHandler.play(playerId);
            } else if (setStatus === "pause") {
                PlayerHandler.pause(playerId);
            }
        } else {
            console.log("Data missing, you must supply playerId and setStatus");
        }
    });

}());