/**
 * Event that is triggered when the current tab has finished loading
 */
window.jQuery(document).ready(function() {
    console.log("Document ready, starting detection of video tags.");

    detectVideoPlayers();

    // When the DOM has been changed then start looking for new video players that might have been created.
    $("body").bind("DOMSubtreeModified", function() {
        detectVideoPlayers();
    });
});

/**
 * Replace all video embeds with our video player (which we can control)
 */
function detectVideoPlayers() {
    $('video').each(function() {
        if (!$(this).hasClass('handled-by-video-chrome-extension')) { //If this doesn't have the class 'handled-by-video-chrome-extension' that means that we have not yet added this video player
            $(this).addClass('handled-by-video-chrome-extension');
            console.log('Found new video player.');
            PlayerHandler.addPlayer(this);
        }
    });
}

/**
 * Event that is triggered when the current tab is closing
 */
$(window).unload(function() {
    console.log("Window unloading, bye now!");
    var playerIds = PlayerHandler.getAllPlayerIds();

    for (var i = 0; i < playerIds.length; i++) {
        var playerId = playerIds[i];
        chrome.extension.sendRequest({
            player: {
                playerId: playerId,
                event: "removed"
            }
        }, function(response) {});
    }
});