package com.github.soshibby.misc;

public class Assert {

	public static void isBoolean(Object variable, String throwMessageOnError) throws Exception{
		if(!(variable instanceof Boolean))
			throw new Exception("Assert failed: " + throwMessageOnError);
	}
	
	public static void isString(Object variable, String throwMessageOnError) throws Exception{
		if(!(variable instanceof String))
			throw new Exception("Assert failed: " + throwMessageOnError);
	}
}
