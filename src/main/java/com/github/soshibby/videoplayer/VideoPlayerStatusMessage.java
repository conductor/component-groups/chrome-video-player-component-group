package com.github.soshibby.videoplayer;

import com.google.gson.annotations.SerializedName;

public class VideoPlayerStatusMessage {
	
	@SerializedName("propertyName")
	public String mPropertyName;
	
	@SerializedName("propertyValue")
	public Object mPropertyValue;
	
	public String getPropertyName(){
		return mPropertyName;
	}
	
	public Object getPropertyValue(){
		return mPropertyValue;
	}
}
