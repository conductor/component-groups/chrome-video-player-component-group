package com.github.soshibby.videoplayer.server;

public interface MessageReceivedListener {
	void onMessageReceived(Object sender, MessageReceivedEvent event);
}
