package com.github.soshibby.videoplayer.server;

public interface SocketCreatedListener {
    void onSocketCreated(Object sender, ClientSocket socket);
}
